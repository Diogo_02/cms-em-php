    <?php
    class Pizza{
        private $id;
        private $nome;
        private $preco;
        private $foto;
        private $type;
        //encapsulamento
        public function setPreco($p){
            $this->preco = ($p != NULL) ? addslashes($p) : NULL;
        }
        public function setId($id){
            $this->id = ($id != NULL) ? addslashes($id) : NULL;
        }
        public function setNome($n){
            $this->nome = ($n != NULL) ? addslashes($n) : NULL;
        }
        public function setFoto($f){
            $this->foto = ($f != NULL) ? addslashes($f) : NULL;
        }
        public function setType($t){
            $this->type = ($t != NULL) ? addslashes($t) : NULL;
        }
        
        

        public function getPreco(){
            return $this->preco;
        }
        public function getId(){
            return $this->id;
        }
        public function getNome(){
            return $this->nome;
        }
        public function getFoto(){
            return $this->foto;
        }
        public function getType(){
            return $this->type;
        }
    } 
?>