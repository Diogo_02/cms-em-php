<?php
require_once ("modelo/pizza.class.php");
require_once ("pizzaControle.class.php");
require_once ("logoControle.class.php");
require_once ("tituloControle.class.php");
require_once ("videoControle.php");
echo "
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Home Admin</title>
    <!-- Bootstrap core CSS -->
    <link href='css/bootstrap.css' rel='stylesheet'>
    <!-- Custom styles for this template -->
    <link href='css/album.css' rel='stylesheet'>
    <!-- Custom styles menu pizza -->

    <link rel='stylesheet' type='text/css' href='css/carousel.css'>
  </head>
  <body>
    <header>
      <div class='navbar navbar-light bg-light shadow-sm'>
        <div id='mySidenav' class='sidenav'>";
          $log = new ControleLogo();
          foreach($log->consultaTodos() as $item){          
          echo "
          <img src=\"retornarLogo.php?id={$item->getId()}\">";}
          echo "
          <a href='elogo.php'>Mudar menu superior</a>
          <a><font color='white'>Bem-Vindo, administrador </font></a><br>
          <a href='javascript:void(0)' class='closebtn' onclick='closeNav()'>&times;</a>
          <a href='#'>Contato</a>
          <a href='#'>Contact</a>
          <a href='logout.php'> Sair</a>
        </div>
        <div id='main'>
          <span style='font-size:18px;cursor:pointer' onclick='openNav()'>&#9776; Menu</span>
        </div>";
        foreach($log->consultaTodos() as $item){          
        echo "
        <img width='90px;' src=\"retornarLogo.php?id={$item->getId()}\"> 
        <label>{$item->getNome()}</label>";}
        echo "
      </div>
    </header>";
    $co = new ControleTitulo();
    foreach($co->consultaTodos() as $item){
    echo "
    <main role='main'>
      <section class='jumbotron text-center' style='background-image:url(\"pgFT.php?id={$item->getId()}\")'>
        <div class='container'>
          <h1 class='jumbotron-heading'>{$item->getTitulo()}</h1>
          <p class='lead text-muted'>{$item->getSub()}</p>";}
          echo "
          <div id='carregar' >
            <button id='dj1' class='btn btn-warning'> Deseja modificar ?</button>
          </div>
          <p class='lead text-muted'>Inicie seu pedido:<br>
            <a href='#' class='btn btn-danger my-2'>Entrega</a>
            <a href='#' class='btn btn-danger my-2'>Retirar na loja</a>
          </p>
        </div>
      </section>
        <nav class='navbar navbar-light bg-light'>
          <div id='tx'>
            <!-- animacaooo ------------- -->
            <img src='imagens/ft.png' id='um'>
            <img src='imagens/ft.png' id='dois'>
            <span class='navbar-brand'>Pizzas</span>
          </div>
        </nav>
        <div class='album py-5 bg-dark'>
          <div class='container'>
            <div class='row'>
              <div class='col-md-4'>
                <div class='card mb-4 shadow-sm'>
                  <img class='card-img-top' src='imagens/frango.jpg' >
                  <div class='card-body'>
                    <p class='card-text'>The Best .</p>
                    <div class='d-flex justify-content-between align-items-center'>
                      <div class='btn-group'>
                        <a class='btn btn-outline-info' role='button' href=atualizaPizza.php >Editar</a>
                        <a class='btn btn-outline-info' role='button' href=apagaPizza.php >Apagar </a>
                      </div>
                      <small class='text-muted'>R$99,00</small>
                    </div>
                  </div>
                </div>
              </div>
            ";
            session_start();
            if(isset($_SESSION['erro'])){
                echo "<script>swal('{$_SESSION['erro']}', {
                    icon: 'success'
                    });</script>";
                session_destroy();
            }
            $comando = new ControlePizza();
            foreach($comando->consultaTodos() as $item){
              echo "
            <div class='col-md-4'>
              <div id='fotos' class='card mb-4 shadow-sm'>
                <img class='card-img-top' src='retornarFoto.php?id={$item->getId()}' >
                <div class='card-body'>
                  <p class='card-text'>{$item->getId()} - {$item->getNome()}</p>
                  <div class='d-flex justify-content-between align-items-center'>
                    <div class='btn-group'>
                      <a class='btn btn-outline-info' role='button' href=\"atualizaPizza.php?id={$item->getId()}\" >Editar </a>
                      <a class='btn btn-outline-info' role='button' href=\"apagaPizza.php?id={$item->getId()}{$item->getNome()}{$item->getPreco()}\" >Apagar </a>
                    </div>
                    <small class='text-muted'>R\${$item->getPreco()},00</small>
                  </div>
                </div>
              </div>
            </div>";
            }
            echo "
            <div class='col-md-4' >
              <div class='card mb-4 shadow-sm'>
                <form method='post' action='processa.php' enctype='multipart/form-data'>
                  <div class='card-body'>
                    <label for='imagem'> Coloque a foto:</label>
                    <input type='file' name='foto' id='foto' required/><br><br>
                    <input type='text' class='form-control' placeholder='nome da pizza' id='nome' name='nome' required/><br><br>
                    <div class='d-flex justify-content-between align-items-center'>
                      <label for='dinheiro'>R$</label><input type='text' id='dinheiro' name='dinheiro' class='dinheiro form-control' style='display:inline-block;' placeholder='preço' required />  
                    </div>
                    <br><Br>
                    <center>
                      <input type='submit' value='Enviar' class='btn btn-primary'  name='enviar' id='enviar' />
                    </center>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <nav class='navbar navbar-light bg-light'>
          <span class='navbar-brand'>Fale Conosto</span>
        </nav>";
        $vi = new ControleVideo();
        foreach($vi->consultaTodos() as $key){
        echo "
        <div class='album py-5 bg-dark'>
          <div class='container'>    
            <div class='col-md-4'>
              <div class='card mb-4 shadow-sm'>
                <div class='card-body' >
                  <embed src='retornarVideo.php?id={$key->getId()}' autoplay='false'></embed>
                  <div class='d-flex justify-content-between align-items-center' id='carrega'>
                    <a href=\"apagaVideo.php?id={$key->getId()}\"> Apagar</a>
                    <button id='dj2' class='btn btn-warning'> Deseja modificar ?</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>";}
        echo "
      </div>
    </main>
    <footer class='text-muted'>
      <div class='container'>
        <p class='float-right'>
          <a href='#'>Back to top</a>
        </p>
      </div>  
    </footer>
    <!-- Bootstrap core JavaScript
    <!-- Placed at the end of the document so the pages load faster -->
    <script src='js/jquery-3.3.1.slim.min.js'></script>
    <script src='js/bootstrap.js'></script>
    <script src='js/ajax.js'></script>
    <script src='js/app.js'></script>
    <script src='js/sweetalert.min.js'></script>
    <script src='js/alerts.js'></script>
    <script src='https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js'></script>           
    <script src='js/processing.js'></script>
    <script>$('.dinheiro').mask('#.##0,00', {reverse: true});</script>
  </body>
</html>
";
?>