<?php
    try{
        require_once("pizzaControle.class.php");
        $executa = new ControlePizza();
        $pizza = new Pizza();
        $pizza->setId($_POST['id']);
        $pizza->setNome($_POST['nome']);
        $pizza->setPreco($_POST['preco']);
        $pizza->setFoto($_FILES['foto']['tmp_name']);
        $pizza->setType($_FILES['foto']['type']);
        if($executa->atualizaPizza($pizza)){
            session_start();
            $_SESSION['erro'] = "deu certo";
            header("Location: admin.php");        

        }else{
            throw new Exception("Erro ao inserir.");
        }
    }catch(Exception $e){
        session_start();
        $_SESSION['erro'] = $e->getMessage();
        echo "error";
        header("Location: admin.php");        
    }
?>