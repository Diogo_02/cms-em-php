<?php
require_once ("sql/Conexao.class.php");
require_once ("modelo/titulo.class.php");
final class ControleTitulo{
	public function consultaTodos(){
	    $conexao = new Conexao("../sql/confi.ini");
        //COMANDO SQL PARA SELECIONAR OS DADOS
        $sql = "SELECT * FROM titulos";
        $comando = $conexao->getConexao()->prepare($sql);
        //executa o comando sql
        $comando->execute();
        $resu = $comando->fetchAll();
        //faz a varredura do array
        $lista = array();
        foreach($resu as $item){
            $titulo = new Titulo();
            $titulo->setId($item->id);               
            $titulo->setTitulo($item->titulo);
            $titulo->setSub($item->sub);
            $titulo->setFoto($item->foto);
            $titulo->setTipo($item->tipo);
            array_push($lista, $titulo);
        }
        $conexao->__destruct();
        return $lista;
    }

	public function adicionarTitulo($titulo){
        $imagemT = explode("/", $titulo->getTipo());
        $imagemT = $imagemT[1];
        $imagemB = file_get_contents($titulo->getFoto());
        //faz a conexao
	    $conexao = new Conexao("../sql/confi.ini");
	    //COMANDO SQL PARA INSERIR OS DADOS
	    $sql = "INSERT INTO titulos VALUES (null,:tit, :sub,:fo,:ti)";
	    //prepara para ser modificada pelo php
	    $comando = $conexao->getConexao()->prepare($sql);
	    //substitue os valores de referencia para os valores das variaveis do titulo
	    $comando->bindParam(":tit",$titulo->getTitulo());
        $comando->bindParam(":sub",$titulo->getSub());
        $comando->bindParam(":fo",$titulo->getFoto());
        $comando->bindParam(":ti",$titulo->getTipo());
	    //executa o comando sql
	    if($comando->execute()){
	        $conexao->__destruct();
	        return true;
	    }else{
	        $conexao->__destruct();
	        return false;
	    }
    }

    public function atualizarTitulo($titulo){
        $imagemT = explode("/", $titulo->getTipo());
        $imagemT = $imagemT[1];
        $imagemB = file_get_contents($titulo->getFoto());    	
        $conexao = new Conexao("../sql/confi.ini");
        $sql = "UPDATE titulos SET id=:id,titulo=:tit, sub=:su, foto=:fo, tipo=:ti";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(":id", $titulo->getId());
   	    $comando->bindValue(":tit",$titulo->getTitulo());
   	    $comando->bindValue(":su",$titulo->getSub());
        $comando->bindParam(":fo", $imagemB);
        $comando->bindParam(":ti", $imagemT);
   	    $comando->execute();
        return true;
    }

    public function selecionaId($id){
	    $conexao = new Conexao("../sql/confi.ini");
        //seleciona o titulo pelo id
        $sql = "SELECT * FROM titulos";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(":id", $id);
        $comando->execute();
        $resu = $comando->fetch();
        $titulo = new Titulo();
        $titulo->setId($resu->id);
        $titulo->setTitulo($resu->titulo);
        $titulo->setSub($resu->sub);
        $conexao->__destruct();
        return $titulo;
    }

}

?>