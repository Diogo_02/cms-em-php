<?php
require_once ("sql/Conexao.class.php");
require_once ("modelo/video.class.php");

final class ControleVideo{
	public function consultaTodos(){
        $conexao = new Conexao("../sql/confi.ini");
        //COMANDO SQL PARA SELECIONAR OS DADOS
        $sql = "SELECT * FROM video";
        $comando = $conexao->getConexao()->prepare($sql);
        //executa o comando sql
        $comando->execute();
        $resu = $comando->fetchAll();
        //faz a varredura do array
        $lista = array();
        foreach($resu as $item){
            $vide = new Video();
            $vide->setId($item->id);               
            $vide->setTipo($item->tipo);
            $vide->setVideo($item->video);
            array_push($lista, $vide);
        }
        $conexao->__destruct();
        return $lista;
    }

    public function selecionarId($id){
        $conexao = new Conexao("../sql/confi.ini");
        //seleciona o video pelo id
        $sql = "SELECT * FROM video WHERE id=:id";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(":id", $id);
        $comando->execute();
        $resu = $comando->fetch();
        $video = new Video();
        $video->setId($resu->id);
        $video->setVideo($resu->video);
        $video->setTipo($resu->tipo);
        $conexao->__destruct();
        return $video;
    }

    public function adicionarVideo($video){
        $videoT = explode("/", $video->getTipo());
        $videoT = $videoT[1];
        $videoB = file_get_contents($video->getVideo());
        $conexao = new Conexao("../sql/confi.ini");
        $sql = "INSERT INTO video VALUES (null,:vi,:ti)";
        //prepara para ser modificada pelo php
        $comando = $conexao->getConexao()->prepare($sql);
        //substitue os valores de referencia para os valores das variaveis do video
        $comando->bindParam(":vi",$videoB);
        $comando->bindParam(":ti",$videoT);
        //executa o comando sql
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }

    public function deletaVideo($id){
        $conexao = new Conexao("../sql/confi.ini");
        //deleta pizza
        $del = $conexao->getConexao()->prepare("DELETE FROM video WHERE id=:id");
        $del->bindValue(":id",$id);
        if($del->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }

    public function atualizarVideo($video){
        $videoT = explode("/", $video->getTipo());
        $videoT = $videoT[1];
        $videoB = file_get_contents($video->getVideo());
        $conexao = new Conexao("../sql/confi.ini");
         $sql = "UPDATE video(video,tipo) SET video=:vi, tipo=:ti WHERE id=:id)";
        //prepara para ser modificada pelo php
        $comando = $conexao->getConexao()->prepare($sql);
        //substitue os valores de referencia para os valores das variaveis do video
        $comando->bindParam(":vi",$videoB->getVideo());
        $comando->bindParam(":ti",$videoT->getTipo());
        //executa o comando sql
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
}

?>