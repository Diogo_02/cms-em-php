<?php
echo "
<!doctype html>
<html >
  <head>
    <meta charset='utf-8'>
    <title>Cadastrar</title>

    <!-- Bootstrap core CSS -->
    <link href='css/bootstrap.css' rel='stylesheet'>

    <!-- Custom styles for this template -->
    <link href='css/signin.css' rel='stylesheet'>
  </head>

  <body class='text-center'>
    <form class='form-signin' method='post' action='cad.php'>
      <img class='mb-4' src='imagens/log.png' alt='' width='72' height='72'>
      <h1 class='h3 mb-3 font-weight-normal'>Faça seu cadastro!</h1>
      <label for='login' class='sr-only'>Nome de usuário:</label>
      <input type='text' id='login' name='login' class='form-control' placeholder='Login' required autofocus>
      <label for='senha' class='sr-only'>Senha</label>
      <input type='password' id='senha' name='senha' class='form-control' placeholder='Senha' required><br>
      <button class='btn btn-lg btn-success btn-block' type='submit' id='cadastrar' name='cadastrar'>Cadastrar</button>
      <a href='index.php'>Já tenho uma conta.</a>
      <p class='mt-5 mb-3 text-muted'>&copy; -2018-</p>
    </form>
  </body>
</html>
";
?>