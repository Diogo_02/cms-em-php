<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Livro</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php
        require_once("lib/controle/LivroControle.class.php");
        $comando = new ControleLivro();
        if(!empty($_POST['titulo'])){
            $comando->atualizaLivro($_GET['id'], $_POST['titulo'], $_POST['tema'], $_POST['paginas']);
            header("Location: pesquisaLivro.php");
        }
    ?>
    <div id="form">
        <form action="processa.php" method="post">
        <?php $resu = $comando->selecionarId($_GET['id'])?>
            <p>Adicionar livro</p>
            <label for="titulo">Titulo</label>
            <input type="text" name="titulo" value="<?php echo "{$resu->getTitulo()}"?>" id="titulo" /><br /> <br />
            <label for="paginas">Paginas</label>
            <input type="text" name="paginas" value="<?php echo "{$resu->getPaginas()}" ?>" id="paginas" /><br /> <br />
            <label for="tema">Tema</label>
            <input type="text" name="tema" value="<?php echo "{$resu->getTema()} "?>" id="tema"/><br /> <br />
            <input type="submit" name="enviar" value="atualizar"/>
            <a type="submit"  class="enviar" href="pesquisaLivro.php">Pesquisar Livro</a>
            <a type="submit"  class="enviar" href="index.php">Adicionar Livro</a>
        </form>
    </div>
</body>
</html>