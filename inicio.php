<?php
require_once ("modelo/pizza.class.php");
require_once ("pizzaControle.class.php");
require_once ("tituloControle.class.php");
require_once ("comControle.class.php");
require_once ("logoControle.class.php");
require_once ("modelo/com.class.php");

echo "
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Home</title>
    <!-- Bootstrap core CSS -->
    <link href='css/bootstrap.css' rel='stylesheet'>
    <!-- Custom styles for this template -->
    <link href='css/album.css' rel='stylesheet'>
    <link rel='stylesheet' type='text/css' href='css/carousel.css'>
</head>
<body>
  <header>
    <div class='navbar navbar-light bg-light shadow-sm'>
      <div id='mySidenav' class='sidenav'>
        <a href='javascript:void(0)' class='closebtn' onclick='closeNav()'>&times;</a>
            ";

$log = new ControleLogo();
foreach($log->consultaTodos() as $item){          
          echo "
            <img src=\"retornarLogo.php?id={$item->getId()}\"> 
            ";
}
if (isset($_COOKIE['login'])) {
  echo"
        <a><font color='white'>Bem-Vindo, {$_COOKIE['login']} </font></a>";
  echo "<a href='logout.php'> Sair</a>";
}else{
  echo "
  <a href='index.php'>Logar</a>";
}
  echo "
        <a href='#'>Contato</a>
        <a href='#'>Contact</a>
      </div>
      <div id='main'>
        <span style='font-size:18px;cursor:pointer' onclick='openNav()'>&#9776; Menu</span>
      </div>
";  foreach($log->consultaTodos() as $item){          
        echo "
        <img width='90px;' src=\"retornarLogo.php?id={$item->getId()}\"> 
        <label>{$item->getNome()}</label>
        ";
  }
echo "
    </div>
  </header>
  <main role='main'>
      ";
$com = new ControleTitulo();
foreach($com->consultaTodos() as $item){
echo "
    <section class='jumbotron text-center' style='background-image:url(\"pgFT.php?id={$item->getId()}\")'>
        <h1 class='jumbotron-heading'>{$item->getTitulo()}</h1>
        <p class='lead text-muted'>{$item->getSub()}</p>      
      ";
}
echo "
        <p class='lead text-muted'>Inicie seu pedido:<br>
          <a href='#' class='btn btn-danger my-2'>Entrega</a>
          <a href='#' class='btn btn-danger my-2'>Retirar na loja</a>
        </p>
      </div>
    </section>
    <nav class='navbar navbar-light bg-light'>
      <div id='tx'>
        <!-- animacaooo ------------- -->
        <img src='imagens/ft.png' id='um'>
        <img src='imagens/ft.png' id='dois'>
      <span class='navbar-brand'>Pizzas</span>
      </div>
    </nav>
    <div class='album py-5 bg-dark'>
      <div class='container'>
        <div class='row'>
          <div class='col-md-4'>
            <div class='card mb-4 shadow-sm'>
              <img class='card-img-top' src='imagens/frango.jpg' >
              <div class='card-body'>
                <p class='card-text'>TheBest</p>
                <div class='d-flex justify-content-between align-items-center'>
                  <div class='btn-group'>
                    <button type='button' class='btn btn-outline-info'>Comprar</button>
                  </div>
                  <small class='text-muted'>R$10,00</small>
                </div>
              </div>
            </div>
          </div>
          ";
session_start();
if(isset($_SESSION['erro'])){
  echo "
          <script>(\"{$_SESSION['erro']}\")</script>";
  session_destroy();
}
$comando = new ControlePizza();
foreach($comando->consultaTodos() as $item){
  echo "
          <div class='col-md-4'>
            <div class='card mb-4 shadow-sm'>
              <img class='card-img-top' src='retornarFoto.php?id={$item->getId()}' >
              <div class='card-body'>
                <p class='card-text'>{$item->getId()} - {$item->getNome()}</p>
                <div class='d-flex justify-content-between align-items-center'>
                  <div class='btn-group'>
                    <button type='button' class='btn btn-outline-info'>Comprar</button>
                  </div>
                  <small class='text-muted'>R\${$item->getPreco()},00</small>
                </div>
              </div>
            </div>
          </div>";
}
echo "
        </div>
        <nav class='navbar navbar-light bg-light'>
          <div id='tx'>
            <!-- animacaooo ------------- -->
            <img src='imagens/ft.png' id='um'>
            <img src='imagens/ft.png' id='dois'>
            <span class='navbar-brand'>informações</span>
          </div>
        </nav>
        <div class='album py-5 bg-dark'>
          <div class='container'>    
            <div class='row'>
              <div class='col-md-4'>
                <div class='card mb-4 shadow-sm'>
                  <div class='card-body'>
                    <label style='font-size:20px;cursor:pointer' >Assista ao vídeo:</label><br>              
                    <embed src='imagens/pizza.mp4' autoplay='false' ></embed>
                    <div class='d-flex justify-content-between align-items-center'>
                    </div>
                  </div>
                </div>
              </div>
              <div class='col-md-4'>
                <div class='card mb-4 shadow-sm'>
                  <div class='card-body'>
                    <div class='d-flex justify-content-between align-items-center'>
                      <label style='font-size:20px;cursor:pointer' >Faça seu comentários sobre nós:<label ><br>              
                      <form method='post' action='comp.php'>
                        <div class='input-group'>
                          <div class='input-group-prepend'>
                            <input type='text' class='form-control' id='come' name='come' placeholder='Comentários...' />
                          </div>
                          <button type='submit' class='input-group-text'>Enviar</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </main>
  <footer class='text-muted'>
    <div class='container'>
      <p class='float-right'>
        <a href='#'>Back to top</a>
      </p>
    </div>  
  </footer>
</body>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src='js/jquery-3.3.1.slim.min.js'></script>

    <script src='js/bootstrap.js'></script>
    <script src='https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js'></script>           
    <script src='js/processing.js'></script>
        <script src='js/sweetalert.min.js'></script>
    <script>
    $('.dinheiro').mask('#.##0,00', {reverse: true});
    </script>
    <script>
      swal('Seja Bem-Vindo(a), {$_COOKIE['login']}!', {
        icon: 'success'
      });
    </script>
</html>
";
?>