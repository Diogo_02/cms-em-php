drop SCHEMA IF EXISTS CMS;
CREATE SCHEMA IF NOT EXISTS CMS;
use CMS;
CREATE TABLE IF NOT EXISTS Usuarios(
    id  INT AUTO_INCREMENT NOT NULL,
    login varchar(200) NOT NULL,
    senha varchar(200) NOT NULL,
    nivel int,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Video(
    id int NOT NULL ,
    tipo varchar(6) NOT NULL,
    video LONGBLOB NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Titulos(
    id INT AUTO_INCREMENT NOT NULL,
    titulo varchar(200) NOT NULL,
    sub varchar(20) NOT NULL,
    foto LONGBLOB NOT NULL,
    tipo VARCHAR(6) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Pizzas(
    id INT AUTO_INCREMENT NOT NULL ,
    nome VARCHAR(200),
    preco VARCHAR(200),
    foto LONGBLOB NOT NULL,
    type varchar(6) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Logo(
    id int NOT NULL AUTO_INCREMENT,
    foto LONGBLOB NOT NULL,
    tipo varchar(6) NOT NULL,
    nome varchar(55) NOT NULL,
    PRIMARY KEY(id)
);
