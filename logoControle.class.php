<?php
require_once ("sql/Conexao.class.php");
require_once ("modelo/logo.class.php");
final class ControleLogo{
	public function consultaTodos(){
	    $conexao = new Conexao("../sql/confi.ini");
        //COMANDO SQL PARA SELECIONAR OS DADOS
        $sql = "SELECT * FROM logo";
        $comando = $conexao->getConexao()->prepare($sql);
        //executa o comando sql
        $comando->execute();
        $resu = $comando->fetchAll();
        //faz a varredura do array
        $lista = array();
        foreach($resu as $item){
            $logo = new Logo();
            $logo->setId($item->id);
            $logo->setNome($item->nome);               
            $logo->setFoto($item->foto);
            $logo->setTipo($item->tipo);
            array_push($lista, $logo);
        }
        $conexao->__destruct();
        return $lista;
    }

	public function adicionarLogo($logo){
        $imagemT = explode("/", $logo->getTipo());
        $imagemT = $imagemT[1];
        $imagemB = file_get_contents($logo->getFoto());
        //faz a conexao
	    $conexao = new Conexao("../sql/confi.ini");
	    //COMANDO SQL PARA INSERIR OS DADOS
	    $sql = "INSERT INTO logo VALUES (:fo,:ti,:no)";
	    //prepara para ser modificada pelo php
	    $comando = $conexao->getConexao()->prepare($sql);
	    //substitue os valores de referencia para os valores das variaveis do logo
	    $comando->bindParam(":fo",$imagemB);
        $comando->bindParam(":ti",$imagemT);
	    $comando->bindValue(":no", $logo->getNome());
        //executa o comando sql
	    if($comando->execute()){
	        $conexao->__destruct();
	        return true;
	    }else{
	        $conexao->__destruct();
	        return false;
	    }
    }

    public function atualizarLogo($logo){
        $imagemT = explode("/", $logo->getTipo());
        $imagemT = $imagemT[1];
        $imagemB = file_get_contents($logo->getFoto());    	
        $conexao = new Conexao("../sql/confi.ini");
        $sql = "UPDATE logo SET id=1,foto=:fo, tipo=:ti,nome=:no";
        $comando = $conexao->getConexao()->prepare($sql);
        //$comando->bindValue(":id", $logo->getId());
        $comando->bindParam(":fo", $imagemB);
        $comando->bindParam(":ti", $imagemT);
        $comando->bindParam(":no", $logo->getNome());
   	    $comando->execute();
        return true;
    }

    public function selecionaId($id){
	    $conexao = new Conexao("../sql/confi.ini");
        //seleciona o logo pelo id
        $sql = "SELECT * FROM logos";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(":id", $id);
        $comando->execute();
        $resu = $comando->fetch();
        $logo = new Titulo();
        $logo->setId($resu->id);
        $logo->setTitulo($resu->logo);
        $logo->setSub($resu->sub);
        $conexao->__destruct();
        return $logo;
    }

}

?>