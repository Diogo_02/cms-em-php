<?php
    try{
        require_once("tituloControle.class.php");
        $executa = new ControleTitulo();
        $titulo = new Titulo();
        $titulo->setTitulo($_POST['titulo']);
        $titulo->setSub($_POST['sub']);
        $titulo->setFoto($_FILES['imagem']['tmp_name']);
        $titulo->setTipo($_FILES['imagem']['type']);
        if ($executa->consultaTodos() == NULL) {
            $executa->adicionarTitulo($titulo);
            session_start();
            $_SESSION['erro'] = "swal('Seja Bem-Vindo, administrador!', {
                icon: 'success'
            });";
            header("Location: admin.php");        
        }else{
            $executa->atualizarTitulo($titulo);
            session_start();
            $_SESSION['erro'] = "swal('Seja Bem-Vindo, administrador!', {
                icon: 'success'
            });";
            header("Location: admin.php");        
        }
    }catch(Exception $e){
        session_start();
        $_SESSION['erro'] = $e->getMessage();
        echo "error";
        header("Location: admin.php");        
    }
?>