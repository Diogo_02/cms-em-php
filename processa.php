<?php
    try{
        require_once("pizzaControle.class.php");
        $executa = new ControlePizza();
        $pizza = new Pizza();
        $pizza->setNome($_POST['nome']);
        $pizza->setPreco($_POST['dinheiro']);
        $pizza->setFoto($_FILES['foto']['tmp_name']);
        $pizza->setType($_FILES['foto']['type']);
        if($executa->adicionarPizza($pizza)){
            session_start();
            echo "<script src='js/sweetalert.min.js'></script>
                <script>
                swal(\"Pizza adicionada!\", {
                    icon: \"success\"
                });
                </script>";
            header("Location: admin.php");        

        }else{
            throw new Exception("Erro ao inserir.");
        }
    }catch(Exception $e){
        session_start();
        $_SESSION['erro'] = $e->getMessage();
        echo "error";
        header("Location: admin.php");        
    }
?>