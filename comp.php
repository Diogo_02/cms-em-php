<?php
    try{
        require_once("comControle.class.php");
        $executa = new ControleComent();
        $com = new Com();
        $com->setComent($_POST['come']);
        $com->setNome_usu($_COOKIE['login']);
        if($executa->adicionarCom($com)){
            session_start();
            echo "<script src='js/sweetalert.min.js'></script>
                <script>
                swal(\"Pizza adicionada!\", {
                    icon: \"success\"
                });
                </script>";
            header("Location: admin.php");        

        }else{
            throw new Exception("Erro ao inserir.");
        }

    }catch(Exception $e){
        session_start();
        $_SESSION['erro'] = $e->getMessage();
        echo "error";
        header("Location: inicio.php");        
    }
?>