<?php
require_once ("sql/Conexao.class.php");
require_once ("modelo/com.class.php");

final class ControleComent{
	public function consultaTodos(){
	    $conexao = new Conexao("../sql/confi.ini");
        //COMANDO SQL PARA SELECIONAR OS DADOS
        $sql = "SELECT * FROM comentario";
        $comando = $conexao->getConexao()->prepare($sql);
        //executa o comando sql
        $comando->execute();
        $resu = $comando->fetchAll();
        //faz a varredura do array
        $lista = array();
        foreach($resu as $item){
            $comen = new Com();
            $comen->setId($item->id);               
            $comen->setData($item->data);
            $comen->setUsu($item->nome);
            $comen->setCom($item->coment);
            array_push($lista, $comen);
        }
        $conexao->__destruct();
        return $lista;
    }

    public function adicionarCom($comen){
        //faz a conexao
        $conexao = new Conexao("../sql/confi.ini");
        //COMANDO SQL PARA INSERIR OS DADOS
        $sql = "INSERT INTO comentario VALUES (null,:no,:da,:co)";
        $comando = $conexao->getConexao()->prepare($sql);
        $comando->bindValue(":no",$comen->getNome_usu());
        $comando->bindValue(":da",$comen->getData());
        $comando->bindValue(":co",$comen->getComent());
        //executa o comando sql
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }




}
?>